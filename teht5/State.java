package teht5;

public class State {

    private int state;

    private static final State programState = new State();

    private State() {
        this.state = 0;
    }

    public static State getInstance() {
        return programState;
    }

    public void nextState() {
        this.state++;
        System.out.println("Program new state: " + this.state);
    }
}