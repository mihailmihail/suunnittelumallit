package teht19;

public interface Mediator {
    void lisääHyppaajä(Hyppääjä hyppääjä);
    boolean seuraavaHyppy();
}
