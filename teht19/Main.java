package teht19;

public class Main {

    public static void main(String[] args) {
        MediatorImpl mediator = new MediatorImpl();
        Hyppääjä mikko = new Hyppääjä(mediator, "Mikko");
        Hyppääjä hannu = new Hyppääjä(mediator, "Hannu");
        Hyppääjä jari = new Hyppääjä(mediator, "Jari");
        Hyppääjä eemeli = new Hyppääjä(mediator, "Eemeli");
        Hyppääjä reiska = new Hyppääjä(mediator, "Reiska");

        boolean kisatKäynnissä = true;
        while (kisatKäynnissä) {
            kisatKäynnissä = mediator.seuraavaHyppy();
        }




    }
}
