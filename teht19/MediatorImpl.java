package teht19;

import java.util.ArrayList;

public class MediatorImpl implements Mediator {
    private int kierros = 0;
    private int seuraavaHyyppääjä = 0;
    private ArrayList<Hyppääjä> hyppääjät = new ArrayList<>();
    private Arvostelutuomari[] tuomarit;
    private Mittamies mittamies = new Mittamies();
    private Kisasihteeri kisasihteeri = new Kisasihteeri();
    private Tulostaulu tulostaulu = new Tulostaulu();

    public MediatorImpl() {
        this.tuomarit = new Arvostelutuomari[]{
                new Arvostelutuomari(),
                new Arvostelutuomari(),
                new Arvostelutuomari(),
                new Arvostelutuomari(),
                new Arvostelutuomari()
        };
    }

    @Override
    public void lisääHyppaajä(Hyppääjä hyppääjä) {
        this.hyppääjät.add(hyppääjä);
    }

    @Override
    public boolean seuraavaHyppy() {
        if(kierros == 2) {
            System.out.println("Kisa ohi!");
            return false;
        }
        if(seuraavaHyyppääjä < hyppääjät.size()){
           Hyppääjä vuorossa = hyppääjät.get(seuraavaHyyppääjä);
            System.out.println("Vuorossa hyppääjä " + vuorossa.getNimi());
           Hyppy hyppy = new Hyppy();
           mittamies.mittaaHyppy(hyppy);
           int tyylipisteet = 0;
           for(Arvostelutuomari tuomari : tuomarit) {
               tyylipisteet += tuomari.arvosteleHyppy(hyppy);
           }
           hyppy.setTyylipisteet(tyylipisteet / tuomarit.length);
           kisasihteeri.laskePisteet(hyppy);
           tulostaulu.addRivi(new Tulosrivi(vuorossa, hyppy));
           vuorossa.lisääHyppy(hyppy, kierros);
           seuraavaHyyppääjä++;
            return true;
        } else {
            System.out.println("\nKierros ohi");
            System.out.println("Kierroksen tulokset: ");
            tulostaulu.tulosta();
            System.out.println("\nSeuraava kierros alkaa\n");
            seuraavaHyyppääjä = 0;
            kierros++;
            return true;
        }
    }
}
