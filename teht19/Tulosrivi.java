package teht19;

public class Tulosrivi {
    private Hyppääjä hyppääjä;
    private Hyppy hyppy;

    public Tulosrivi(Hyppääjä hyppääjä, Hyppy hyppy) {
        this.hyppääjä = hyppääjä;
        this.hyppy = hyppy;
    }

    public void tulostaRivi() {
        System.out.println("Hyppääjä: " + hyppääjä.getNimi() + " Tulos: " + hyppy.getPisteet());
    }
}
