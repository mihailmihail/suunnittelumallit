package teht19;

public class Kisasihteeri {

    void laskePisteet(Hyppy hyppy) {
        // Patentoitu mäkihyppy pistelaskumenetelmä
        hyppy.setPisteet((hyppy.getTyylipisteet() * 2 + hyppy.getPituus() * 3) / 6);
    }
}
