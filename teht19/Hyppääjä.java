package teht19;

public class Hyppääjä {
    private Hyppy[] hypyt = new Hyppy[2];
    private Mediator mediator;
    private String nimi;

    public Hyppääjä(Mediator mediator, String nimi) {
        this.mediator = mediator;
        this.nimi = nimi;
        mediator.lisääHyppaajä(this);
    }

    void lisääHyppy(Hyppy hyppy, int kierros) {
        hypyt[kierros] = hyppy;
    }

    String getNimi() {
        return this.nimi;
    }
}
