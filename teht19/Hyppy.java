package teht19;

public class Hyppy {
    private int pisteet;    //  ?
    private int pituus;
    private int tyylipisteet;

    public int getPisteet() {
        return pisteet;
    }

    public int getPituus() {
        return pituus;
    }

    public int getTyylipisteet() {
        return tyylipisteet;
    }

    public void setPisteet(int pisteet) {
        this.pisteet = pisteet;
    }

    public void setPituus(int pituus) {
        this.pituus = pituus;
    }

    public void setTyylipisteet(int tyylipisteet) {
        this.tyylipisteet = tyylipisteet;
    }
}
