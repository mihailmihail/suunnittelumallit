package teht19;

import java.util.ArrayList;

public class Tulostaulu {
    private ArrayList<Tulosrivi> tulosrivit = new ArrayList<>();

    public void addRivi(Tulosrivi tulosrivi) {
        this.tulosrivit.add(tulosrivi);
    }

    public void tulosta() {
        for(Tulosrivi tulosrivi : tulosrivit) {
            tulosrivi.tulostaRivi();
        }
    }

}
