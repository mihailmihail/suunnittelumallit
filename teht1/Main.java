package teht1;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus oppilas = new Oppilas();
        AterioivaOtus presidentti = new Presidentti();
        opettaja.aterioi();
        oppilas.aterioi();
        presidentti.aterioi();
    }
}
