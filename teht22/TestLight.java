package teht22;

public class TestLight {
    public static void main(String[] args){
        Light lamp = new Light();
        Valkokangas valkokangas = new Valkokangas();
        Command switchUp = new FlipUpCommand(lamp);
        Command switchDown = new FlipDownCommand(lamp);
        Command valkokangasUp = new ValkokangasUpCommand(valkokangas);
        Command valkokangasDown = new ValkokangasDownCommand(valkokangas);
        WallButton valkokangasButton1 = new WallButton(valkokangasUp);
        WallButton valkokangasButton2 = new WallButton(valkokangasDown);
        WallButton button1 = new WallButton(switchUp);
        WallButton button2 = new WallButton(switchDown);
        button1.push();
        button2.push();
        valkokangasButton1.push();
        valkokangasButton2.push();
    }
}
