package teht22;

public class ValkokangasDownCommand implements Command{

    private Valkokangas valkokangas;

    public ValkokangasDownCommand(Valkokangas valkokangas){
        this.valkokangas = valkokangas;
    }

    @Override// Command
    public void execute(){
        valkokangas.down();
    }
}
