package teht22;

public class ValkokangasUpCommand implements Command{
    private Valkokangas valkokangas;

    public ValkokangasUpCommand(Valkokangas valkokangas){
        this.valkokangas = valkokangas;
    }

    @Override// Command
    public void execute(){
        valkokangas.up();
    }
}
