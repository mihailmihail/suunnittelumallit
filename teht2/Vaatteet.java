package teht2;

public class Vaatteet {

    private Vaate farmarit;
    private Vaate tpaita;
    private Vaate lippis;
    private Vaate kengat;

    Vaatteet(String merkki) {
        farmarit = new Vaate(merkki);
        tpaita = new Vaate(merkki);
        lippis = new Vaate(merkki);
        kengat = new Vaate(merkki);
    }

    private class Vaate {
        String merkki;

        Vaate(String merkki) {
            this.merkki = merkki;
        }

        public String toString() {
            return this.merkki;
        }
    }

    public String toString() {
        return farmarit.toString() + " merkkiset farmarit, " +
                tpaita.toString() + " merkkinen tpaita, " +
                lippis.toString() + " merkkinen lippis ja " +
                kengat.toString() + " merkkiset kengät";
    }
}