package teht2;

public class Main {

    public static void main(String[] args) {
        Vaatteet adidas = new Vaatteet("Adidas");
        Vaatteet boss = new Vaatteet("Hugo Boss");

        Jasper jasper = new Jasper(adidas);
        jasper.luetteleVaatteet();

        jasper.vaihdaVaatteet(boss);

        jasper.luetteleVaatteet();
    }

}
