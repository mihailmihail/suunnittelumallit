package teht7;

public class Pelihahmo {
    private HahmoTaso taso;

    public Pelihahmo() {
        this.taso = Vauva.getInstance();
    }

    public void nostaTasoa() {
        taso.nostaTasoa(this);
    }

    public void laskeTasoa() {
        taso.laskeTasoa(this);
    }

    public void meditoi() {
        taso.meditoi(this);
    }

    protected void vaihdaTasoa(HahmoTaso taso) {
        this.taso = taso;
    }
}
