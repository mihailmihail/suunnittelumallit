package teht7;

public class Jumala extends HahmoTaso {

    private static final Jumala instace = new Jumala();

    private Jumala() {}

    public static Jumala getInstance() {
        return instace;
    }

    @Override
    void nostaTasoa(Pelihahmo hahmo) {
        System.out.println("Olet jo jumala, mitä muuta haluat?\n");
    }

    @Override
    void laskeTasoa(Pelihahmo hahmo) {
        System.out.println("Alistut takaisin kuolevaiseksi.\n");
        vaihdaTasoa(hahmo, Aikuinen.getInstance());
    }

    @Override
    void meditoi(Pelihahmo hahmo) {
        System.out.println("Meditoit ja maailmasta tulee hieman parempi paikka.\n");
    }
}
