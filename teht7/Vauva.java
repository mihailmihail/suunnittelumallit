package teht7;

public class Vauva extends HahmoTaso {

    private static final Vauva instance = new Vauva();

    private Vauva() {}

    public static Vauva getInstance() {
        return instance;
    }

    @Override
    void nostaTasoa(Pelihahmo hahmo) {
        System.out.println("Tunnet kuinka nuoruus jää taaksesi ja aikuisuuden vastuut odottaa edessäpäin.\n");
        vaihdaTasoa(hahmo, Aikuinen.getInstance());
    }

    @Override
    void laskeTasoa(Pelihahmo hahmo) {
        System.out.println("Vauva on alin taso.\n");
    }

    @Override
    void meditoi(Pelihahmo hahmo) {
        System.out.println("Muutut jumalaksi.\n");
        vaihdaTasoa(hahmo, Jumala.getInstance());
    }
}
