package teht7;

public class Aikuinen extends HahmoTaso {

    private static final Aikuinen instance = new Aikuinen();

    private Aikuinen()  {}

    public static Aikuinen getInstance() {
        return instance;
    }

    @Override
    void nostaTasoa(Pelihahmo hahmo) {
        System.out.println("Aikuinen on hahmon viimeinen taso\n");
    }

    @Override
    void laskeTasoa(Pelihahmo hahmo) {
        System.out.println("Nuorrut takaisin taaperoksi.\n");
        vaihdaTasoa(hahmo, Vauva.getInstance());
    }

    @Override
    void meditoi(Pelihahmo hahmo) {
        System.out.println("Muutut jumalaksi!\n");
        vaihdaTasoa(hahmo, Jumala.getInstance());
    }
}
