package teht7;

public abstract class HahmoTaso {

    abstract void nostaTasoa(Pelihahmo hahmo);
    abstract void laskeTasoa(Pelihahmo hahmo);
    abstract void meditoi(Pelihahmo hahmo);

    void vaihdaTasoa(Pelihahmo hahmo, HahmoTaso taso) {
        hahmo.vaihdaTasoa(taso);
    }
}
