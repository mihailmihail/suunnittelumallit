package teht7;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Pelihahmo hahmo = new Pelihahmo();

        int valinta = 0;
        while(valinta != 9){
            System.out.println("Valitse toiminto:");
            System.out.println("1. Nosta hahmon tasoa");
            System.out.println("2. Laske hahmon tasoa");
            System.out.println("3. Meditoi hahmolla");
            System.out.println("9. Lopeta");
            System.out.println("Syötä valinta: ");
            valinta = Lue.kluku();
            switch (valinta) {
                case 1: hahmo.nostaTasoa(); break;
                case 2: hahmo.laskeTasoa(); break;
                case 3: hahmo.meditoi(); break;
            }
        }
    }
}
