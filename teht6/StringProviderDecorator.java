package teht6;

public class StringProviderDecorator implements StringProvider {
    private StringProvider delegate;
    public StringProviderDecorator(StringProvider delegate) {
        this.delegate = delegate;
    }

    @Override
    public char[] getChars() {
        return this.delegate.getChars();
    }
}
