package teht6;

public class EncryptStringProvider extends StringProviderDecorator {

    private final int secret = 2;

    public EncryptStringProvider(StringProvider delegate) {
        super(delegate);
    }

    public String encrypt() {
        char[] chars = super.getChars();
        char[] encrypted = new char[chars.length];
        int i = 0;
        for(char c : chars) {
            encrypted[i++] = (char) (c ^ secret);
        }
        return new String(encrypted);
    }
}
