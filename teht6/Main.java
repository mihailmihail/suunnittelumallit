package teht6;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {

        FileReader reader = new FileReader("src/teht6/test.txt");
        FileWriter writer = new FileWriter("src/teht6/encrypted.txt");
        int c;

        StringBuilder sb = new StringBuilder();
        while((c = reader.read()) != -1) {
            sb.append((char) c);
        }
        reader.close();

        System.out.println("\noriginal: \n" + sb.toString());

        String encryptedValue
                = new EncryptStringProvider(new PlainStringProvider(sb.toString())).encrypt();
        System.out.println("\nencrypted\n " + encryptedValue);

        writer.write(encryptedValue);
        writer.flush();
        writer.close();

        reader = new FileReader("src/teht6/encrypted.txt");

        sb = new StringBuilder();
        while((c = reader.read()) != -1) {
            sb.append((char) c);
        }
        reader.close();

        String decryptedValue
                = new DecryptStringProvider(new PlainStringProvider(sb.toString())).decrypt();
        System.out.println("\ndecrypted: \n" + decryptedValue);

        writer = new FileWriter("src/teht6/decrypted.txt");
        writer.write(decryptedValue);
        writer.flush();
        writer.close();
    }
}
