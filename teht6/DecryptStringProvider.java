package teht6;

public class DecryptStringProvider extends StringProviderDecorator {

    private final int secret = 2;

    public DecryptStringProvider(StringProvider delegate) {
        super(delegate);
    }

    public String decrypt() {
        char[] chars = super.getChars();
        char[] decrypted = new char[chars.length];
        int i = 0;
        for(char c : chars) {
            decrypted[i++] = (char) (c ^ secret);
        }
        return new String(decrypted);
    }
}
