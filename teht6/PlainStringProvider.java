package teht6;

public class PlainStringProvider implements StringProvider {
    private char[] stringChars;
    public PlainStringProvider(String stringValue) {
        this.stringChars = stringValue.toCharArray();
    }

    public char[] getChars() {
        return stringChars;
    }

    public String read() {
        return new String(this.stringChars);
    }
}
