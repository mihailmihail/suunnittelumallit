package teht6;

interface StringProvider {
    char[] getChars();
}