package teht3;

public class Osa implements LaiteOsa {

    private int hinta;

    Osa(int hinta) {
        this.hinta = hinta;
    }

    @Override
    public int getHinta() {
        return this.hinta;
    }
}