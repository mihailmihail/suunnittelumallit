package teht3;

import java.util.ArrayList;
import java.util.Arrays;

public class KoosteOsa extends Osa {

    private ArrayList<Osa> osat = new ArrayList<>();

    KoosteOsa(int hinta) {
        super(hinta);
    }

    public void addOsa(Osa osa) {
        osat.add(osa);
    }

    public void addOsat(Osa[] osat) {
        this.osat.addAll(Arrays.asList(osat));
    }

    public int getHinta() {
        int total = super.getHinta();
        for(Osa osa : osat) {
            total += osa.getHinta();
        }
        return total;
    }
}