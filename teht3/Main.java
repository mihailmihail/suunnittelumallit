package teht3;

public class Main {

    public static void main(String[] args) {
        Osa muistipiiri = new Osa(40);
        Osa prosessori = new Osa(140);
        Osa verkkokortti = new Osa(30);
        Osa naytonohjain = new Osa(400);

        KoosteOsa emolevy = new KoosteOsa(120);
        emolevy.addOsat(new Osa[]{muistipiiri, prosessori, verkkokortti, naytonohjain});

        KoosteOsa kotelo = new KoosteOsa(50);
        kotelo.addOsa(emolevy);

        System.out.println("Koko paketin hinta: " + kotelo.getHinta());
    }
}
