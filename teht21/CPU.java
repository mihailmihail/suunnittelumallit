package teht21;

import java.nio.charset.StandardCharsets;

class CPU {

    private byte[] currentAddress;

    public void freeze() {
        System.out.println("Prosessori jäädytetty");
    }
    public void jump(long position) {
        System.out.println("Hypätään muistiosoitteeseen: " + position);
        currentAddress = Memory.bank.get(position);
    }
    public void execute() {
        String text = new String(currentAddress, StandardCharsets.UTF_8);
        System.out.println("Luetaan muistipaikka...");
        for(int i = 0; i < text.length(); i++) {
            System.out.println("Data: " + text.charAt(i));
        }
    }
}
