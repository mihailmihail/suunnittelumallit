package teht21;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

class HardDrive {

    private HashMap<Long, char[]> bootSectors = new HashMap<>(5);

    public HardDrive() {
        bootSectors.put(1L, generateSector());
        bootSectors.put(2L, generateSector());
        bootSectors.put(3L, generateSector());
        bootSectors.put(4L, generateSector());
        bootSectors.put(5L, generateSector());
    }

    private char[] generateSector() {
        char[] sector = new char[64];
        Random r = new Random();
        for(int i=0; i<sector.length; i++){
            sector[i] = (char)(r.nextInt(26) + 'a');
        }
        return sector;
    }

    public byte[] read(long lba, int size) {
        System.out.println("Ladataan kovalevylta sektori: " + lba);
        System.out.println("Koko: " + size);
        return new String(Arrays.copyOfRange(bootSectors.get(lba), 0, size)).getBytes(StandardCharsets.UTF_8);
    }
}
