package teht21;

import java.util.HashMap;

class Memory {

    static HashMap<Long, byte[]> bank = new HashMap<>(64);

    public void load(long position, byte[] data) {
        System.out.println("Syötetään muistiin dataa kohtaan: " + position);
        bank.put(position, data);
    }
}
