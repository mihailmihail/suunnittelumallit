package teht21;


class ComputerFacade {
    private final CPU processor;
    private final Memory ram;
    private final HardDrive hd;

    public ComputerFacade() {
        this.processor = new CPU();
        this.ram = new Memory();
        this.hd = new HardDrive();
    }

    public void start() {
        processor.freeze();
        System.out.println("Syötä bootti sektori 1-5: ");
        int BOOT_SECTOR = Lue.kluku();
        System.out.println("Syötä luettavan pituus max 64: ");
        int SECTOR_SIZE = Lue.kluku();
        long BOOT_ADDRESS = 1L;
        ram.load(BOOT_ADDRESS, hd.read(BOOT_SECTOR, SECTOR_SIZE));
        processor.jump(BOOT_ADDRESS);
        processor.execute();
    }
}

