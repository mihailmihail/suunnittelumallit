package teht9;

import java.util.List;

public class ThirdStrategy implements ListConverter {

    @Override
    public String listToString(List list) {
        StringBuilder string = new StringBuilder();
        for(int i = 0; i < list.size(); i++) {
            if((i + 1) % 3 == 0) {
                string.append(list.get(i)).append("\n");
            } else {
                string.append(list.get(i));
            }
        }
        return string.toString();
    }
}
