package teht9;

import java.util.List;

public class FirstStrategy implements ListConverter{


    @Override
    public String listToString(List list) {
        StringBuilder string = new StringBuilder();
        for(Object o : list) {
            string.append(o).append("\n");
        }
        return string.toString();
    }
}
