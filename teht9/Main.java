package teht9;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FirstStrategy strategy1 = new FirstStrategy();
        SecondStrategy strategy2 = new SecondStrategy();
        ThirdStrategy strategy3 = new ThirdStrategy();

        List<String> list = Arrays.asList("Ensinmäinen", "Toinen", "Kolmas", "Neljäs", "Viides", "Kuudes", "Seitsemäs");

        System.out.println("\n###First strategy:");
        System.out.println(strategy1.listToString(list));
        System.out.println("\n### Second strategy ###");
        System.out.println(strategy2.listToString(list));
        System.out.println("\n### Third strategy ###");
        System.out.println(strategy3.listToString(list));

    }




}
