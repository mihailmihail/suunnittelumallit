package teht15;

public class Main {

    public static void main(String[] args) {
        PelaajaNoppa pelaajaNoppa = new PelaajaNoppa();
        VihollisNoppa vihollisNoppa = new VihollisNoppa();

        System.out.println("Pelaaja liikkuu eteenpäin: ");
        System.out.println(pelaajaNoppa.liikuPaljon() + " verran");
        System.out.println("Pelaaja kohtaa vihollisen !!");
        System.out.println("Vihollinen lyö: ");
        System.out.println(vihollisNoppa.teeVahinkoaPaljon() + " vahinkopisteen verran");

    }
}
