package teht15;

public class PelaajaNoppa extends Noppa implements Liike {

    public PelaajaNoppa() {
        super(6);
    }

    @Override
    public int liikuPaljon() {
        return this.heitä() + this.heitä();
    }

    @Override
    public int liikuVähän() {
        return this.heitä();
    }
}
