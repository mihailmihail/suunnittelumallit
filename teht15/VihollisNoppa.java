package teht15;

public class VihollisNoppa extends Noppa implements Vahinko{
    public VihollisNoppa() {
        super(12);
    }

    @Override
    public int teeVahinkoaPaljon() {
        return this.heitä() + this.heitä();
    }

    @Override
    public int teeVahinkoaVähän() {
        return this.heitä();
    }
}
