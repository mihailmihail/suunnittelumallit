package teht13;

public class LoitsuvoimaVisitor implements Visitor {

    @Override
    public void visit(Pappihahmo pappihahmo) {

        double loitsuvoima = pappihahmo.getINT();

        switch (pappihahmo.getTila()) {
            case Normaali: loitsuvoima = loitsuvoima * 1.0; break;
            case Myrkytetty: loitsuvoima = loitsuvoima * 0.7; break;
            case Tehostettu: loitsuvoima = loitsuvoima * 1.5; break;
        }
        pappihahmo.setLoitsuvoima(loitsuvoima);
    }

    @Override
    public void visit(Voimamieshahmo voimamieshahmo) {
        double loitsuvoima = voimamieshahmo.getINT();

        switch (voimamieshahmo.getTila()) {
            case Normaali: loitsuvoima *= 0.9; break;
            case Myrkytetty: loitsuvoima *= 0.4; break;
            case Tehostettu: loitsuvoima *= 1.1; break;
        }
        voimamieshahmo.setLoitsuvoima(loitsuvoima);
    }
}
