package teht13;

public class Voimamieshahmo extends Pelihahmo {

    private int INT;
    private int AGI;
    private int STR;

    Voimamieshahmo() {
        this.INT = 10;
        this.AGI = 15;
        this.STR = 20;
    }
    @Override
    public double getSTR() {
        return STR;
    }

    @Override
    public double getAGI() {
        return AGI;
    }

    @Override
    public double getINT() {
        return INT;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
