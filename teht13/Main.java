package teht13;

public class Main {

    public static void main(String[] args) {

        Pelihahmo[] pelihahmot = {new Pappihahmo(), new Voimamieshahmo()};

        LoitsuvoimaVisitor loitsuvoimaVisitor = new LoitsuvoimaVisitor();
        HeittovoimaVisitor heittovoimaVisitor = new HeittovoimaVisitor();

        for(Pelihahmo pelihahmo : pelihahmot) {
            pelihahmo.accept(loitsuvoimaVisitor);
            pelihahmo.accept(heittovoimaVisitor);
        }
        System.out.println("Aloitat pelin pappihahmona");
        System.out.println("Papilla on loitsuvoimaa käytettävissä: " + pelihahmot[0].getLoitsuvoima());
        System.out.println("ja heittovoimaa: " + pelihahmot[0].getHeittovoima());
        System.out.println("\nMutta juotkin vahingossa myrkkyliemen! Voi ei!");
        pelihahmot[0].setTila(Tila.Myrkytetty);

        for(Pelihahmo pelihahmo : pelihahmot) {
            pelihahmo.accept(loitsuvoimaVisitor);
            pelihahmo.accept(heittovoimaVisitor);
        }
        System.out.println("Nyt sinulla on");
        System.out.println("loitsuvoimaa käytettävissä: " + pelihahmot[0].getLoitsuvoima());
        System.out.println("ja heittovoimaa: " + pelihahmot[0].getHeittovoima());

        System.out.println("\nVoimamiehellä on taas alussa");
        System.out.println("loitsuvoimaa: " + pelihahmot[1].getLoitsuvoima());
        System.out.println("ja heittovoimaa: " + pelihahmot[1].getHeittovoima());
        System.out.println("\nMutta valitettavasti voimamieskin juo myrkkyliemen!");
        pelihahmot[1].setTila(Tila.Myrkytetty);

        for(Pelihahmo pelihahmo : pelihahmot) {
            pelihahmo.accept(loitsuvoimaVisitor);
            pelihahmo.accept(heittovoimaVisitor);
        }
        System.out.println("Nyt sinulla on");
        System.out.println("loitsuvoimaa käytettävissä: " + pelihahmot[1].getLoitsuvoima());
        System.out.println("ja heittovoimaa: " + pelihahmot[1].getHeittovoima());
        System.out.println("\nVoimamies kaivaa kuitenkin repustaan superparannusliemen!");
        System.out.println("Voimamiehen voimat ehtyvät jumalaiselle tasolle!");
        pelihahmot[1].setTila(Tila.Tehostettu);

        for(Pelihahmo pelihahmo : pelihahmot) {
            pelihahmo.accept(loitsuvoimaVisitor);
            pelihahmo.accept(heittovoimaVisitor);
        }
        System.out.println("Nyt sinulla on");
        System.out.println("loitsuvoimaa käytettävissä: " + pelihahmot[1].getLoitsuvoima());
        System.out.println("ja heittovoimaa: " + pelihahmot[1].getHeittovoima());
    }
}
