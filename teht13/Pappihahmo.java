package teht13;

public class Pappihahmo extends Pelihahmo {

    private int INT;
    private int AGI;
    private int STR;

    Pappihahmo() {
        this.INT = 20;
        this.AGI = 10;
        this.STR = 10;
    }

    @Override
    public double getSTR() {
        return STR;
    }

    @Override
    public double getAGI() {
        return AGI;
    }

    @Override
    public double getINT() {
        return INT;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
