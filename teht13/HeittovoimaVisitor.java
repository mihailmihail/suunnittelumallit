package teht13;

public class HeittovoimaVisitor implements Visitor {

    @Override
    public void visit(Pappihahmo pappihahmo) {

        double heittovoima = pappihahmo.getAGI() + pappihahmo.getSTR();

        switch (pappihahmo.getTila()) {
            case Normaali: heittovoima *= 0.9; break;
            case Myrkytetty: heittovoima *= 0.5; break;
            case Tehostettu: heittovoima *= 1.1; break;
        }

        pappihahmo.setHeittovoima(heittovoima);
    }

    @Override
    public void visit(Voimamieshahmo voimamieshahmo) {
        double heittovoima = voimamieshahmo.getAGI() + voimamieshahmo.getSTR();

        switch (voimamieshahmo.getTila()) {
            case Normaali: heittovoima *= 1.2; break;
            case Myrkytetty: heittovoima *= 0.8; break;
            case Tehostettu: heittovoima *= 1.5; break;
        }

        voimamieshahmo.setHeittovoima(heittovoima);
    }
}
