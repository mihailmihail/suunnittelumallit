package teht13;

public interface Visitor {
    void visit(Pappihahmo pappihahmo);
    void visit(Voimamieshahmo voimamieshahmo);
}
