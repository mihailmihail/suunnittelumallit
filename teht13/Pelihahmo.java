package teht13;

public abstract class Pelihahmo {

    private Tila tila;

    private double loitsuvoima;
    private double heittovoima;

    Pelihahmo() {
        this.tila = Tila.Normaali;
    }

    public void setLoitsuvoima(double loitsuvoima) { this.loitsuvoima = loitsuvoima; }
    public double getLoitsuvoima() { return loitsuvoima; }
    public void setHeittovoima(double heittovoima) { this.heittovoima = heittovoima; }
    public double getHeittovoima() { return heittovoima; }
    public Tila getTila() { return this.tila; }
    protected void setTila(Tila tila) {
        this.tila = tila;
    }

    abstract public double getSTR();
    abstract public double getAGI();
    abstract public double getINT();

    abstract public void accept(Visitor visitor);
}
