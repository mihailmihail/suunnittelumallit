package teht4;


import java.util.Observable;

class Aika extends Observable implements Runnable {

    private int tunnit;
    private int minuutit;
    private int sekuntit;

    public int getTunnit() { return this.tunnit; }
    public int getMinuutit() { return this.minuutit; }
    public int getSekuntit() { return this.sekuntit; }

    private void tick() {
        sekuntit++;
        if(sekuntit > 59) {
            sekuntit = 0;
            minuutit++;
        }
        if(minuutit > 59) {
            minuutit = 0;
            tunnit++;
        }
        if(tunnit > 23) {
            tunnit = 0;
        }
    }

    @Override
    public void run() {
        while (true) {
            tick();
            setChanged();
            notifyObservers();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}