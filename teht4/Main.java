package teht4;

public class Main {
    public static void main(String[] args) {
        Aika aika = new Aika();
        Kello kello = new Kello();

        aika.addObserver(kello);

        new Thread(aika).start();
    }
}
