package teht4;

import java.util.Observable;
import java.util.Observer;

public class Kello implements Observer{

    @Override
    public void update(Observable o, Object arg) {
        Aika aika = (Aika) o;
        System.out.println("Kello:");
        System.out.println(aika.getTunnit()+":"+aika.getMinuutit()+":"+aika.getSekuntit());
    }
}
