package teht11;

import java.util.Random;

public class Arvuuttaja {

    public Object liityPeliin() {
        return new Memento();
    }

    public boolean arvaa(Object memento, int arvaus) {
        return ((Memento) memento).getLuku() == arvaus;
    }

    private class Memento {
        private int luku;

        private Memento() {
            Random r = new Random();
            luku = r.nextInt(11);
        }
        private int getLuku() { return this.luku; }
    }
}
