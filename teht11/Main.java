package teht11;

public class Main {

    public static void main(String[] args) {

        Arvuuttaja arvuuttaja = new Arvuuttaja();
        Object peli = arvuuttaja.liityPeliin();

        /**
         * Memento antaa luvun väliltä 0 - 10, käydään kaikki luvut läpi.
         */
        for(int i = 0; i < 11; i++) {
            if(arvuuttaja.arvaa(peli, i)) {
                System.out.println("Arvasit oikein! Oikea luku oli: " + i);
            } else {
                System.out.println("Luku " + i + " ei ollut oikea luku. Yritä uudelleen.");
            }
        }

    }
}
