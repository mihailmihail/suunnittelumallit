package teht8;

abstract class Game {
    protected int playersCount;
    abstract void initializeGame();
    abstract boolean makePlay(int player);
    abstract boolean endOfGame();
    abstract void printWinner();

    public final void playOneGame(int playersCount) {
        this.playersCount = playersCount;
        initializeGame();
        int j = 0;
        while(!endOfGame()) {
            if(makePlay(j)) {
                j = (j + 1) % playersCount;
            }
        }
    }
}
