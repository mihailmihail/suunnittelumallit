package teht8.GameObjects;

import teht8.Peli;

import java.awt.*;
import java.awt.geom.Point2D;

public class Projectile extends GameObject {

    private int size = 20;
    private Handler handler;
    private Player player;

    private LineIterator path;

    public Projectile(float x, float y, ObjectID id, Handler handler, Player player) {
        super(x, y, id);
        this.player = player;
        this.handler = handler;
    }

    public void setPath(LineIterator path) {
        this.path = path;
    }

    public Player getPlayer() { return this.player; }

    @Override
    public void tick() {

        Point2D current;
        if(path.hasNext()) {
            current = path.next();
            x = (float) current.getX() - size / 2;
            y = (float) current.getY() - size / 2;
        } else {
            handler.removeObject(this);
        }

        if(y >= Peli.getHeight()) handler.removeObject(this);
        if(x >= Peli.getWidth()) handler.removeObject(this);

        collision();
    }

    @Override
    public void render(Graphics g) {
        g.setColor(player.getColor());
        g.fillRect((int)x,(int) y, size, size);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x,(int) y, size, size);
    }

    private void collision() {
        // Gameobject collision code
        for(int i = 0; i < handler.getObjectSize(); i++) {
            GameObject tempObject = handler.getObject(i);
            if(tempObject.getId() == ObjectID.Projectile && ((Projectile) tempObject).getPlayer() != this.player) {
                if(getBounds().intersects(tempObject.getBounds())){
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.removeObject(this);
                    handler.removeObject(tempObject);
                    i--;
                }
            }
        }
    }
}
