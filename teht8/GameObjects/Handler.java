package teht8.GameObjects;

import teht8.Peli;

import java.awt.*;
import java.util.LinkedList;

public class Handler {

    LinkedList<GameObject> objects = new LinkedList<>();
    LinkedList<Player> players = new LinkedList<>();

    public void tick() {
        for(int i = 0; i < objects.size(); i++){
            GameObject tempObject = objects.get(i);
            tempObject.tick();
        }

        for(int i = 0; i < players.size(); i++) {
            Player tempPlayer = players.get(i);
            tempPlayer.tick();
        }
    }

    public int getObjectSize() { return this.objects.size(); }

    public void render(Graphics g) {
        for(int i = 0; i < objects.size(); i++){
            GameObject tempObject = objects.get(i);
            tempObject.render(g);
        }

        for(int i = 0; i < players.size(); i++) {
            Player tempPlayer = players.get(i);
            tempPlayer.render(g);
        }
    }

    public void clearAll() {
        objects.clear();
        players.clear();
    }

    public void clearMenuParticles() {
        for(int i = 0; i < objects.size(); i++) {
            GameObject tempObject = objects.get(i);
            if(tempObject.id == ObjectID.MenuParticle) {
                removeObject(tempObject);
                i--;
            }
        }
    }

    public void endTurn() {
        for(int i = 0; i < players.size(); i++) {
            players.get(i).shootTurret();
        }

        for(int i = 0; i < objects.size(); i++) {
            GameObject tempObject = objects.get(i);
            if(tempObject.id == ObjectID.Trajectory) {
                ((Trajectory) tempObject).reset();
            }
        }
    }

    public void clearPlayers() {
        this.players.clear();
    }

    public void addObject(GameObject object) {
        this.objects.add(object);
    }

    public GameObject getObject(int index) {
        return objects.get(index);
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public Player getPlayer(int index) {
        return players.get(index);
    }

    public void removePlayer(Player player) {
        this.players.remove(player);
        if(this.players.size() == 1) {
            Peli.gamestate = Peli.GAMESTATE.End;
        }
    }

    public void removeObject(GameObject object) {
        this.objects.remove(object);
    }
}
