package teht8.GameObjects;

import java.awt.*;
import java.util.Random;

public class CollisionParticle extends GameObject {

    private final int LIFESPAN = 30000;
    private int renderCount = 0;
    private int size = 5;
    private Color color;
    private Handler handler;
    Random r = new Random();

    public CollisionParticle(float x, float y, ObjectID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;
        this.color = new Color(255,255,0);
        velX = r.nextInt(5 - -5) + -5;
        velY = r.nextInt(5 - -5) + -5;
    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        if(renderCount >= LIFESPAN) {
            handler.removeObject(this);
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(color);
        g.fillRect((int)x,(int) y, 16, 16);
        renderCount++;
    }

    @Override
    public Rectangle getBounds() {
        return null;
    }
}
