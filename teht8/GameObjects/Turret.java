package teht8.GameObjects;

import java.awt.*;

public class Turret extends GameObject {

    private int size;

    private Trajectory trajectory;
    private Handler handler;
    private Player player;

    public Turret(float x, float y, int size, Handler handler, Player player) {
        super(x, y, ObjectID.Turret);
        this.handler = handler;
        this.size = size;
        this.player = player;
        trajectory = new Trajectory(x + size / 2, y + size / 2, ObjectID.Trajectory);
        handler.addObject(trajectory);
    }

    public void set(Point point) {
        trajectory.setEndpoint(point);
    }

    public void shoot () {
        Projectile projectile = new Projectile(x + size / 2, y + size / 2, ObjectID.Projectile, handler, player);
        projectile.setPath(trajectory.getLineIterator());
        handler.addObject(projectile);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.drawRect((int) x, (int) y, size, size);
        trajectory.render(g);
    }

    @Override
    public Rectangle getBounds() {
        return null;
    }
}
