package teht8.GameObjects;

import java.awt.*;
import java.awt.geom.Line2D;

public class Trajectory extends GameObject {

    private static final double LINEPRECISION = 4.0;

    private int x2;
    private int y2;
    private boolean show = true;

    public Trajectory(float x1, float y1, ObjectID id) {
        super(x1, y1, id);
        x2 = (int) x1;
        y2 = (int) y1;
    }

    public Trajectory(float x1, float y1, float x2, float y2, ObjectID id) {
        this(x1, y1, id);
        this.x2 = (int) x2;
        this.y2 = (int) y2;
    }

    public void setX2(int x2) { this.x2 = x2; }
    public void setY2(int y2) { this.y2 = y2; }
    public void setEndpoint(Point point) { this.x2 = point.x; this.y2 = point.y; }
    public LineIterator getLineIterator() { return new LineIterator(new Line2D.Float(x, y, x2, y2), LINEPRECISION); }

    public void show() {
        this.show = true;
    }

    public void reset() {
        this.x2 = (int) x;
        this.y2 = (int) y;
    }

    @Override
    public void tick() {}

    @Override
    public void render(Graphics g) {
        if(show) g.drawLine((int) super.x, (int) super.y, x2, y2);
    }

    @Override
    public Rectangle getBounds() {
        return null;
    }
}
