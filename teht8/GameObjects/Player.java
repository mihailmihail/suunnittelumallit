package teht8.GameObjects;

import teht8.Peli;

import java.awt.*;

public class Player extends GameObject {

    private static int currentPlayer = 0;
    private int turretSize = 40;

    private Color playerColor;
    private Turret turret;
    private Handler handler;

    private boolean done = false;
    private boolean shooting = false;

    public Player(float x, float y, ObjectID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;
        this.playerColor = nextColor();
        this.turret = new Turret(x, y, turretSize, handler, this);
    }

    public void done(boolean ready) { this.done = ready; }
    public boolean isReady() { return this.done; }
    public void shooting(boolean isShooting) { this.shooting = isShooting; }
    public boolean isShooting() { return this.shooting; }
    public void setTurretSize(int turretSize) { this.turretSize = turretSize; }
    public Color getColor() { return this.playerColor; }

    public static int nextPlayer() {
        currentPlayer = currentPlayer < Peli.playersCount - 1 ? currentPlayer + 1 : 0;
        return currentPlayer;
    }

    public void endTurn() {
        shooting(false);
        done(true);
    }

    public void setTurret(Point point) {
        turret.set(point);
    }

    public void shootTurret() {
        turret.shoot();
    }

    public void explode() {
        System.out.println("BUM!");
    }

    @Override
    public void tick() {
        collision();
    }

    @Override
    public void render(Graphics g) {
        g.setColor(playerColor);
        turret.render(g);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x,(int) y, turretSize, turretSize);
    }

    private void collision() {
        for(int i = 0; i < handler.getObjectSize(); i++) {
            GameObject tempObject = handler.getObject(i);
            if(tempObject.getId() == ObjectID.Projectile && ((Projectile) tempObject).getPlayer() != this) {
                //collision code
                if(getBounds().intersects(tempObject.getBounds())){
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.addObject(new CollisionParticle(x, y, ObjectID.CollisionParticles, handler));
                    handler.removeObject(tempObject);
                    handler.removePlayer(this);
                }
            }
        }
    }

    private static Color nextColor() {
        final Color[] colors = {Color.RED, Color.BLUE, Color.YELLOW, Color.GREEN};
        return colors[  nextPlayer() ];
    }
}
