package teht8.GameObjects;

public enum ObjectID {
    MenuParticle(),
    Player(),
    Trajectory(),
    Turret(),
    Projectile(),
    CollisionParticles(),
}
