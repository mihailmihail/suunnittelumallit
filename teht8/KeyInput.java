package teht8;

import teht8.Menu.MainMenu;
import teht8.Menu.PauseMenu;
import teht8.Menu.VictoryMenu;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

    MainMenu mainMenu;
    PauseMenu pauseMenu;
    VictoryMenu victoryMenu;

    public KeyInput(MainMenu mainMenu, PauseMenu pauseMenu, VictoryMenu victoryMenu) {
        this.mainMenu = mainMenu; this.pauseMenu = pauseMenu; this.victoryMenu = victoryMenu;
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if(Peli.gamestate == Peli.GAMESTATE.MainMenu) {
            if(key == KeyEvent.VK_UP) { this.mainMenu.up(); }
            if(key == KeyEvent.VK_DOWN) { this.mainMenu.down(); }
            if(key == KeyEvent.VK_ENTER) { this.mainMenu.select(); }
        } else if (Peli.gamestate == Peli.GAMESTATE.Game) {
            if(key == KeyEvent.VK_ESCAPE) { Peli.gamestate = Peli.GAMESTATE.Paused; }
        } else if (Peli.gamestate == Peli.GAMESTATE.Paused) {
            if(key == KeyEvent.VK_UP) { this.pauseMenu.up(); }
            if(key == KeyEvent.VK_DOWN) { this.pauseMenu.down(); }
            if(key == KeyEvent.VK_ENTER) { this.pauseMenu.select(); }
            if(key == KeyEvent.VK_ESCAPE) { Peli.gamestate = Peli.GAMESTATE.Game; }
        } else if (Peli.gamestate == Peli.GAMESTATE.VictoryMenu) {
            if(key == KeyEvent.VK_ENTER) { this.victoryMenu.select(); }
        }
    }
}
