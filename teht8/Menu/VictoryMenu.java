package teht8.Menu;

import teht8.Peli;

import java.awt.*;

public class VictoryMenu extends Menu {

    private final String TOMAINMENU = "TO MAIN MENU";

    private String winner = " wins the game";

    private Peli peli;

    public VictoryMenu(Peli peli) {
        super(1);
        this.peli = peli;
        super.buttons[0] = new Button(TOMAINMENU);
        super.buttons[0].toggleSelect();
    }

    public void setWinner(String playerName) {
        String winnerString = " player wins the game";
        this.winner = playerName + winnerString;
    }

    @Override
    public void render(Graphics g) {
        g.setFont(new Font(Menu.FONTFAMILY, Font.PLAIN, 40));
        g.setColor(Color.WHITE);
        g.drawString(winner, Peli.getWidth() / 2 - 300, 50);
        g.drawString(super.buttons[0].getTitle(), Peli.getWidth() / 2 - 300, 150);
    }

    @Override
    public void select() {
        switch (super.selected) {
            case 0:
                Peli.gamestate = Peli.GAMESTATE.MainMenu; peli.loadMenu(); break;
        }
    }
}
