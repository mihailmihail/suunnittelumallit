package teht8.Menu;

import teht8.Peli;

public class MainMenu extends Menu {

    private final String PLAYSTRING = "P L A Y";
    private final String OPTIONSSTRING = "O P T I O N S";
    private final String EXITSTRING = "E X I T";

    private Peli peli;

    public MainMenu(Peli peli) {
        super();
        this.peli = peli;
        super.buttons[0] = new Button(PLAYSTRING);
        super.buttons[1] = new Button(OPTIONSSTRING);
        super.buttons[2] = new Button(EXITSTRING);
        super.buttons[0].toggleSelect();
    }

    public void select() {
        switch (super.selected) {
            case 0:
                Peli.gamestate = Peli.GAMESTATE.Game; peli.newGame(); break;
            case 1:
                System.out.println("Options"); break;
            case 2:
                Peli.gamestate = Peli.GAMESTATE.Exit; break;
        }
    }
}
