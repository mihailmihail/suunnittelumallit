package teht8.Menu;

import java.awt.*;

public abstract class Menu {

    protected Button[] buttons;
    protected int selected = 0;

    private final String MENUSTRING = "M E N U";

    protected static final String FONTFAMILY = "arial";
    private final Font MENUFONT = new Font(FONTFAMILY, Font.PLAIN, 50);
    private final Font MENUSELECTEDFONT = new Font(FONTFAMILY, Font.PLAIN, 70);

    public Menu(int buttonCount) {
        this.buttons = new Button[buttonCount];
    }

    public Menu() {
        this(3);
    }

    public void render(Graphics g) {

        int yPos = 50;
        g.setColor(Color.CYAN);
        g.drawString(MENUSTRING, Button.Position.MediumCenter.calcX(), yPos);

        yPos += 100;
        for(Button button : buttons) {
            g.setFont(MENUFONT);
            g.setColor(Color.DARK_GRAY);
            if(button.isSelected()){
                g.setFont(MENUSELECTEDFONT);
                g.setColor(Color.WHITE);
                g.drawString(button.getTitle(), button.position.calcX(), yPos);
            } else {
                g.drawString(button.getTitle(), button.position.calcX(), yPos);
            }
            yPos += 100;
        }
    }

    public void down() {
        if(selected < 2) {
            buttons[selected].toggleSelect();
            selected++;
            buttons[selected].toggleSelect();
        }
    }

    public void up() {
        if(selected > 0) {
            buttons[selected].toggleSelect();
            selected--;
            buttons[selected].toggleSelect();
        }
    }

    public abstract void select();


}
