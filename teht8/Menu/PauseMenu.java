package teht8.Menu;

import teht8.Peli;

public class PauseMenu extends Menu{

    private final String CONTINUESTRING = "R E S U M E";
    private final String OPTIONSSTRING = "O P T I O N S";
    private final String EXITSTRING = "M E N U";

    private Peli peli;

    public PauseMenu(Peli peli) {
        super();
        this.peli = peli;
        super.buttons[0] = new Button(CONTINUESTRING);
        super.buttons[1] = new Button(OPTIONSSTRING);
        super.buttons[2] = new Button(EXITSTRING);
        super.buttons[0].toggleSelect();
    }

    @Override
    public void select() {
        switch (super.selected) {
            case 0:
                Peli.gamestate = Peli.GAMESTATE.Game; break;
            case 1:
                System.out.println("Options"); break;
            case 2:
                Peli.gamestate = Peli.GAMESTATE.MainMenu; peli.loadMenu(); break;
        }
    }
}
