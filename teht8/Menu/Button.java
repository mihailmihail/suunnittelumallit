package teht8.Menu;

import teht8.Peli;

public class Button {

    private String title;
    private boolean selected;
    protected Position position;

    protected enum Position {
        MediumCenter(180),
        LargeCenter(180);

        private final int offset;

        Position(int offset) {
            this.offset = offset;
        }

        int calcX() {
            return Peli.getWidth() / 2 - this.offset;
        }
    }

    Button(String title) {
        this.title = title;
        this.position = Position.MediumCenter;
    }

    public String getTitle() { return this.title; }
    public void setTitle(String title) { this.title = title; }
    public void toggleSelect() {
        this.selected = !this.selected;
        this.position = this.selected ? Position.LargeCenter : Position.MediumCenter;
    }
    public boolean isSelected() { return this.selected; }
}
