package teht8;

import java.util.function.Function;

public class Engine {

    private long lastTime = System.nanoTime();
    private double amountOfTicks = 60.0;
    private double ns = 1000000000 / amountOfTicks;
    private double delta = 0;
    private long timer = System.currentTimeMillis();
    private int frames = 0;

    private Peli peli;

    protected boolean running;

    Engine(Peli peli) {
        this.peli = peli;
    }

    public void run() {
        // Game loop
        long now = System.nanoTime();
        delta += (now - lastTime) / ns;
        lastTime = now;
        while(delta >= 1){
            peli.tick();
            delta--;
        }
        if(running) {
            peli.render();
        }
        frames++;

        if(System.currentTimeMillis() - timer > 1000) {
            timer += 1000;
            // System.out.println("FPS " + frames);
            frames = 0;
        }
    }
}
