package teht8;

import teht8.GameObjects.*;
import teht8.Menu.MainMenu;
import teht8.Menu.PauseMenu;
import teht8.Menu.VictoryMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferStrategy;
import java.lang.reflect.Field;
import java.util.concurrent.ThreadLocalRandom;

public class Peli extends Game implements Runnable {

    public static GAMESTATE gamestate;

    public enum GAMESTATE {
        MainMenu, VictoryMenu, End,
        Paused, Exit, Game
    }

    public static int playersCount;

    private Engine engine;
    private Canvas canvas;

    private Handler handler;
    private MainMenu mainMenu;
    private PauseMenu pauseMenu;
    private VictoryMenu victoryMenu;
    private KeyInput keyInput;
    private MouseInput mouseInput;
    private MouseMovement mouseMovement;

    private static int width = 1024;
    private static int height = width / 12 * 9;

    private Player currentPlayer;
    private Boolean running;

    private final int PARTICLECOUNT = 40;
    private int turnCounter = 0;
    private int totalTurns = 0;

    public Peli(int playersCount) {
        // Set the player count
        Peli.playersCount = playersCount;
        // Init components
        this.engine = new Engine(this);
        this.handler = new Handler();
        gamestate = GAMESTATE.MainMenu;
        this.mainMenu = new MainMenu(this);
        this.pauseMenu = new PauseMenu(this);
        this.victoryMenu = new VictoryMenu(this);
        // Init inputs
        this.keyInput = new KeyInput(this.mainMenu, this.pauseMenu, this.victoryMenu);
        this.mouseInput = new MouseInput();
        this.mouseMovement = new MouseMovement();
        // Init graphics
        this.canvas = new Canvas();
        this.canvas.addKeyListener(this.keyInput);
        this.canvas.addMouseListener(this.mouseInput);
        this.canvas.addMouseMotionListener(this.mouseMovement);
        // Load the meny
        this.loadMenu();
    }

    public static int getWidth() { return width; }
    // public static void setWidth(int width) { Peli.width = width; }
    public static int getHeight() { return height; }
    // public static void setHeight(int height) { Peli.height = height; }

    public synchronized void start(){
        Thread thread = new Thread(this);
        thread.start();
        engine.running = true;
    }

    @Override
    public void run() {
        playOneGame(playersCount);
    }

    protected void render(){
        BufferStrategy bs = this.canvas.getBufferStrategy();
        if (bs == null){
            this.canvas.createBufferStrategy(3);
            return;
        }

        Graphics g = bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, width, height);

        if(gamestate == GAMESTATE.Game || gamestate == GAMESTATE.MainMenu) {
            handler.render(g);
        }

        if(gamestate == GAMESTATE.VictoryMenu) {
            victoryMenu.render(g);
        }

        if(gamestate == GAMESTATE.MainMenu) {
            mainMenu.render(g);
        }

        if(gamestate == GAMESTATE.Paused) {
            pauseMenu.render(g);
        }
        g.dispose();
        bs.show();
    }

    protected void tick() {
        handler.tick();
        if(gamestate == GAMESTATE.Exit) {
            System.exit(0);
        }
        if(gamestate == GAMESTATE.End) {
            printWinner();
        }
        if(gamestate == GAMESTATE.Game) {
            if(turnCounter == playersCount) {
                totalTurns += turnCounter;
                handler.endTurn();
                turnCounter = 0;
            }
        }
    }

    @Override
    void initializeGame() {
        JFrame frame = new JFrame("Peli");
        frame.setPreferredSize(new Dimension(width, height));
        frame.setMaximumSize(new Dimension(width, height));
        frame.setMinimumSize(new Dimension(width, height));

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.add(this.canvas);
        frame.setVisible(true);
    }

    @Override
    boolean makePlay(int player) {
        if(currentPlayer == null) {
            return false;
        }
        if(currentPlayer.isReady()){
            currentPlayer.done(false);
            setCurrentPlayer(handler.getPlayer(Player.nextPlayer()));
            return true;
        } else {
            return false;
        }
    }

    @Override
    boolean endOfGame() {
        if(engine.running) {
            engine.run();
            return false;
        } else {
            return true;
        }
    }

    @Override
    void printWinner() {
        Color winnerColor = handler.getPlayer(0).getColor();
        // Very hacky way of getting string representation of Color
        for (Field f : Color.class.getFields()){
            try {
                if(f.getType() == Color.class && f.get(null).equals(winnerColor)) {
                    victoryMenu.setWinner(f.getName());
                }
            } catch (java.lang.IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        gamestate = GAMESTATE.VictoryMenu;
    }

    public void newGame() {
        handler.clearAll();

        for(int i = 0; i < playersCount; i++) {
            Point position = nextPlayerSpot(i);
            handler.addPlayer(new Player(position.x, position.y, ObjectID.Player, handler));
        }
        setCurrentPlayer(handler.getPlayer(0));
    }

    private void addMenuParticles(int amount){
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for(int i = 0; i < amount; i++){
            handler.addObject(new MenuParticle(random.nextInt(width), random.nextInt(width), ObjectID.MenuParticle, handler));
        }
    }

    private void setCurrentPlayer(Player player) {
        this.currentPlayer = player;
        player.shooting(true);
    }

    public void loadMenu() {
        handler.clearAll();
        this.addMenuParticles(PARTICLECOUNT);
    }

    private Point nextPlayerSpot(int playerIndex) {
        switch (playerIndex){
            case 0:
                return new Point(width / 2 - 10, 50);
            case 1:
                return new Point(width / 2 - 10, 650);
            case 2:
                return new Point(50, height / 2 - 10);
            case 3:
                return new Point(650, height / 2 - 10);
            default:
                return null;
        }
    }

    class MouseMovement extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent e) {
            if(gamestate == GAMESTATE.Game) {
                if(currentPlayer.isShooting()){
                    currentPlayer.setTurret(e.getPoint());
                }
            }
        }
    }

    class MouseInput extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            if(gamestate == GAMESTATE.Game) {
                currentPlayer.setTurret(e.getPoint());
                currentPlayer.endTurn();
                turnCounter++;
            }
        }
    }
}
