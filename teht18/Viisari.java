package teht18;

public class Viisari implements Cloneable{

    private int aika;

    public Viisari() {
        aika = 0;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void nollaa() {
        this.aika = 0;
    }

    public void lisää() {
        this.aika++;
    }

    public int getAika() {
        return this.aika;
    }

}
