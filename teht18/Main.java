package teht18;

public class Main {

    public static void main(String[] args) {

        Kello kello = new Kello("Kello 1");
        Kello kello2 = kello.clone();
        kello2.setTitle("Kello 2");


        Thread t1 = new Thread(kello);
        Thread t2 = new Thread(kello2);

        t1.start();
        t2.start();
    }
}
