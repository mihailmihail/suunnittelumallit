package teht18;

public class Kello implements Runnable, Cloneable{

    private String title;

    private Viisari sekunnit;
    private Viisari minuutit;
    private Viisari tunnit;

    public Kello(String title) {
        this.title = title;
        sekunnit = new Viisari();
        minuutit = new Viisari();
        tunnit = new Viisari();
    }

    public Kello clone() {
        Kello k = null;

        try {
            k = (Kello) super.clone();
            k.sekunnit = (Viisari) sekunnit.clone();
            k.minuutit = (Viisari) minuutit.clone();
            k.tunnit = (Viisari) tunnit.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return k;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private void tick() {
        sekunnit.lisää();
        if(sekunnit.getAika() > 59) {
            sekunnit.nollaa();
            minuutit.lisää();
        }
        if(minuutit.getAika() > 59) {
            minuutit.nollaa();
            tunnit.lisää();
        }
        if(tunnit.getAika() > 23) {
            tunnit.nollaa();
        }
    }

    @Override
    public void run() {
        while (true) {
            tick();
            System.out.println(this.title + " on " + tunnit.getAika() + ":" + minuutit.getAika() + ":" + sekunnit.getAika());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
