package teht10;

public class ImmediateSupervisor extends PayRise {

    private final double ALLOWEDRAISE = BASE;

    @Override
    public void processRequest(PayRiseRequest request) {
        if(request.getCurrentPay() * ALLOWEDRAISE > request.getRequestAmount() - request.getCurrentPay()) {
            System.out.println("Your immediate supervisor accepts your requested $" + request.getRequestAmount() + " pay rise");
        } else if (successor != null) {
            successor.processRequest(request);
        }
    }
}
