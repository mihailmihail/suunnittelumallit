package teht10;

public class PayRiseRequest {

    private double currentPay;
    private double requestedAmount;

    public PayRiseRequest(double currentPay, double request) {
        this.currentPay = currentPay;
        this.requestedAmount = request;
    }

    double getCurrentPay() { return this.currentPay; }
    double getRequestAmount() { return this.requestedAmount; }
}
