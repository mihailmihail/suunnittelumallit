package teht10;

public abstract class PayRise {

    protected static final double BASE = 0.02;
    protected PayRise successor;

    public void setSuccessor(PayRise successor) {
        this.successor = successor;
    }

    abstract public void processRequest(PayRiseRequest request);
}
