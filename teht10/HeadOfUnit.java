package teht10;

public class HeadOfUnit extends PayRise {

    // 0.02 * 2.5 = 0.05
    private final double ALLOWEDRAISE = BASE * 2.5;

    @Override
    public void processRequest(PayRiseRequest request) {
        if(request.getCurrentPay() * ALLOWEDRAISE > request.getRequestAmount() - request.getCurrentPay()) {
            System.out.println("Head of the unit accepts your requested $" + request.getRequestAmount() + " pay rise");
        } else if (successor != null) {
            successor.processRequest(request);
        }
    }
}
