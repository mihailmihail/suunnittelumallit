package teht10;

public class Main {

    public static void main(String[] args) {

        ImmediateSupervisor supervisor = new ImmediateSupervisor();
        HeadOfUnit headOfUnit = new HeadOfUnit();
        CEO ceo = new CEO();

        supervisor.setSuccessor(headOfUnit);
        headOfUnit.setSuccessor(ceo);

        System.out.println("Työntekijä 1 pyytää 2000e palkkaansa 1.9% korotusta (2038)");
        supervisor.processRequest(new PayRiseRequest(2000, 2038));
        System.out.println("Työntekijä 2 pyytää 2000e palkkaansa 4.2% korotusta (2084)");
        supervisor.processRequest(new PayRiseRequest(2000, 2084));
        System.out.println("Työntekijä 3 pyytää 2000e palkkaansa naurettavat 11% korotusta (2220)");
        supervisor.processRequest(new PayRiseRequest(2000, 2220));

    }
}
