package teht14;

import java.util.Arrays;
import java.util.List;

public class BigMac extends BurgerBuilder {

    private List<String> osat =  Arrays.asList(
            "ketsuppi", "sipuli", "majoneesi", "salaatti", "juusto", "pihvi");

    public BigMac() {
        super.createNewBurger();
        burger.setOsat(osat);
    }

    @Override
    public void buildAines1() {
        burger.setAines1(osat.get(0));
    }

    @Override
    public void buildAines2() {
        burger.setAines2(osat.get(1));
    }

    @Override
    public void buildAines3() {
        burger.setAines3(osat.get(2));
    }

    @Override
    public void buildAines4() {
        burger.setAines4(osat.get(3));
    }

    @Override
    public void buildAines5() {
        burger.setAines5(osat.get(4));
    }

    @Override
    public void buildAines6() {
        burger.setAines6(osat.get(5));
    }

    @Override
    public void buildAines7() {
        burger.setAines7(osat.get(5));
    }
}
