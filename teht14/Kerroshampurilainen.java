package teht14;

public class Kerroshampurilainen extends BurgerBuilder {

    private StringBuilder stringBuilder;

    public Kerroshampurilainen () {
        super.createNewBurger();
        stringBuilder = new StringBuilder();
        stringBuilder.append("ketsuppi\n");
        stringBuilder.append("sipuli\n");
        stringBuilder.append("suolakurkku\n");
        stringBuilder.append("salaatti\n");
        stringBuilder.append("juusto\n");
        stringBuilder.append("pihvi\n");
        burger.setOsat(stringBuilder);
    }

    @Override
    public void buildAines1() {
        burger.setAines1("ketsuppi");
    }

    @Override
    public void buildAines2() {
        burger.setAines2("sipuli");
    }

    @Override
    public void buildAines3() {
        burger.setAines3("suolakurkku");
    }

    @Override
    public void buildAines4() {
        burger.setAines4("salaatti");
    }

    @Override
    public void buildAines5() {
        burger.setAines5("juusto");
    }

    @Override
    public void buildAines6() {
        burger.setAines6("pihvi");
    }

    @Override
    public void buildAines7() {
        burger.setAines7("pihvi");
    }
}
