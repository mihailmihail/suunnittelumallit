package teht14;

public class Tarjoilija {

    private BurgerBuilder burgerBuilder;

    public void setBurgerBuilder(BurgerBuilder burgerBuilder) {
        this.burgerBuilder = burgerBuilder;
    }

    public Object getBurger() {
        return this.burgerBuilder.getBurger();
    }

    public void rakennaBurgeri() {
        burgerBuilder.buildAines1();
        burgerBuilder.buildAines2();
        burgerBuilder.buildAines3();
        burgerBuilder.buildAines4();
        burgerBuilder.buildAines5();
        burgerBuilder.buildAines6();
        burgerBuilder.buildAines7();
    }
}
