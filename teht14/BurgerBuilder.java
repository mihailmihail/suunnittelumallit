package teht14;

public abstract class BurgerBuilder {
    protected Burger burger;

    public Object getBurger() {
        return burger.getOsat();
    }

    public void createNewBurger() {
        burger = new Burger();
    }

    public abstract void buildAines1();
    public abstract void buildAines2();
    public abstract void buildAines3();
    public abstract void buildAines4();
    public abstract void buildAines5();
    public abstract void buildAines6();
    public abstract void buildAines7();
}
