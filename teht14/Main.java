package teht14;

public class Main {

    public static void main(String[] args) {
        Tarjoilija tarja = new Tarjoilija();
        BurgerBuilder kerrohampurilainen = new Kerroshampurilainen();
        BurgerBuilder bigMac = new BigMac();

        tarja.setBurgerBuilder(kerrohampurilainen);
        System.out.println("Tarja rakentaa burgeria...");
        tarja.rakennaBurgeri();
        System.out.println("Burgeri valmis!");
        System.out.println("Burgerin osat:");
        System.out.println(((StringBuilder) tarja.getBurger()).toString());

        tarja.setBurgerBuilder(bigMac);
        System.out.println("\nTarja rakentaa uutta burgeria...");
        tarja.rakennaBurgeri();
        System.out.println();
        System.out.println("Burgeri valmis!");
        System.out.println("Burgerin osat:");
        System.out.println(tarja.getBurger().toString());
    }
}
